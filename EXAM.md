# CSCM602023 Advanced Programming (KKI) - 2020 - Final Programming Exam

> Priagung Satria Wicaksana - 1806241160

TODO: Write the answers related to the programming exam here.

Task 1: Ensuring 12 Factors


I. Codebase
One codebase tracked in revision control, many deploys
    - it is tracked by gitlab
    
    
II. Dependencies
Explicitly declare and isolate dependencies
    - lombok dependency
    
III. Config
Store config in the environment
    
    
IV. Backing services
Treat backing services as attached resources
    
    
V. Build, release, run
Strictly separate build and run stages
    - .gitlab-ci.yml
    
VI. Processes
Execute the app as one or more stateless processes
    
    
VII. Port binding
Export services via port binding
    
    
VIII. Concurrency
Scale out via the process model
    
    
IX. Disposability
Maximize robustness with fast startup and graceful shutdown
    
    
X. Dev/prod parity
Keep development, staging, and production as similar as possible
    
    
XI. Logs
Treat logs as event streams
    
    
XII. Admin processes
Run admin/management tasks as one-off processes
    